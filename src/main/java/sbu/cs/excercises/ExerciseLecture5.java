package sbu.cs.excercises;

public class ExerciseLecture5 {

    /**
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     * @param length length of the wanted password
     */
    public String weakPassword(int length) {
        return null;
    }

    /**
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     * @param length length of the wanted password
     */
    public String strongPassword(int length){
        return null;
    }

    /**
     *   implement a function that checks if an integer is a fibobin number
     *   integer n is fibobin if there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     * @param n the number n
     */
    public boolean isFiboBin(int n) {
        return false;
    }
}
