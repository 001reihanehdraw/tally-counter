public class Tally
{
    private int value = 0;

    public void count()
    {
        if (value != 9999)
        {
            value++;
        }
    }

    public int getValue()
    {
        return value;
    }

    public void reset()
    {
        this.value = 0;
    }

}
